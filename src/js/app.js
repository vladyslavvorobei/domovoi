import $ from 'jquery';
import Swiper from 'swiper';
import '../../node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min';
import '../../node_modules/jquery-mask-plugin/dist/jquery.mask.min';
import '../../node_modules/jquery-validation/dist/jquery.validate.min';
import '../../node_modules/jquery-popup-overlay/jquery.popupoverlay';


$(window).scroll(function() {
  // - - - - SCROOL - - - -
  if($(this).scrollTop() > 70) {
    $('.header').addClass('scroll');
  }
  else {
    $('.header').removeClass('scroll');
  }
});

$(window).on('load',function() {
  if ($(window).width() < 992) {
    $('.callback a.modal_open').html('Оставить заявку');
  }
  else {
    $('.callback a.modal_open').html('Обратный звонок');
  }
});


var swiper = new Swiper('.reviews__slider .swiper-container', {
  slidesPerView: 2,
  spaceBetween: 30,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  breakpoints: {
    991: {
      slidesPerView: 1,
      spaceBetween: 40,
    },
  }
});
var swiperTwo = new Swiper('.about__slider .swiper-container', {
  spaceBetween: 30,
  pagination: {
    el: '.swiper-pagination',
  },
  breakpoints: {
    991: {
      slidesPerView: 1,
      spaceBetween: 80,
    },
  }
});


$(document).ready(function() {

  // - - - - ANCHORN - - - -
  $('.scroll-down').on('click', function(event) {
    var target = $(this.getAttribute('href'));

    if (target.length) {
      event.preventDefault();
      $('html, body').stop().animate({
        scrollTop: target.offset().top
      }, 1000);
    }
  });
  // - - - - MODAL - - - -
  $('.modal').popup({
    transition: 'all 0.3s',
    outline: true, // optional
    focusdelay: 400, // optional
    vertical: 'top', //optional
    // onclose: function() {
    //   $(this).find('label.error').remove();
    // }
  });
  // - - - - NAV-BURGER - - - -
  $('.nav__button').click(function() {
    $('.header').toggleClass('active');
    $('.nav__button').toggleClass('active');
    $('.header__hamburger').toggleClass('active');
  });
  // - - - - FAQ-BURGER - - - -
  $('.faq__head').click(function() {
    if ($(this).hasClass('active')) {
      $(this).removeClass('active');
    }
    else {
      $('.faq__head').removeClass('active');
      $(this).addClass('active');
    }
    $('.faq__toggle').addClass('.active');
    $(this).next().slideToggle();
    $('.faq__toggle').not($(this).next()).slideUp();
  });
  // - - - - CITY-MENU - - - -
  $('.city-menu button').on('click', function() {
    $('.city-menu').toggleClass('active');
    $(document).mouseup(function(e) {
      let div = $('.city-menu');
      if (!div.is(e.target)
        && div.has(e.target).length === 0) {
        $('.city-menu').removeClass('active');
      }
    });
  });
  // - - - - NAV-MENU - - - -
  $('.nav button').on('click', function() {
    $('.nav').toggleClass('active');
    $(document).mouseup(function(e) {
      let div = $('.nav');
      if (!div.is(e.target)
        && div.has(e.target).length === 0) {
        $('.nav').removeClass('active');
      }
    });
  });

  // - - - - JQUERY MASK + VALIDATE - - - -
  $('input[type="tel"]').mask('+7 (000) 000-00-00');
  jQuery.validator.addMethod('phoneno', function(phoneNumber, element) {
    return this.optional(element) || phoneNumber.match(/\+[0-9]{1}\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}/);
  }, 'Введите Ваш телефон');

  $('.form').each(function(index, el) {
    $(el).addClass('form-' + index);

    $('.form-' + index).validate({
      rules: {
        name: 'required',
        agree: 'required',
        tel: {
          required: true,
          phoneno: true
        }
      },
      messages: {
        name: 'Неправильно введенное имя',
        tel: 'Неправильно введен телефон',
        agree: 'Нужно соглашение на обработку данных',
      },
      submitHandler: function(form) {
        var t = $('.form-' + index).serialize();
        ajaxSend('.form-' + index, t);
      }
    });
  });
  function ajaxSend(formName, data) {
    jQuery.ajax({
      type: 'POST',
      url: 'sendmail.php',
      data: data,
      success: function() {
        $('.modal').popup('hide');
        $('#thanks').popup('show');
        setTimeout(function() {
          $(formName).trigger('reset');
        }, 2000);
      }
    });
  };
});

$(window).resize(function() {
  if ($(window).width() >1200) {
    $('nav').removeClass('active');
    $('city-menu').removeClass('active');
  }
  if ($(window).width() < 992) {
    $('.callback a.modal_open').html('Оставить заявку');
  }
  else {
    $('.callback a.modal_open').html('Обратный звонок');
  }
});
